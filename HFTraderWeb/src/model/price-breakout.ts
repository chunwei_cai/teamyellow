import { Position } from "./position";
import { Trader } from "./trader";

/**
 * Serializable/deserializable encapsulation of a BB strategy,
 * including its trading history, profitability, and ROI.
 *
 * @author Chunwei Cai
 */
export class PriceBreakers extends Trader {
  windowsize: number;
  exitThreshold: number;

  constructor(ID: number, stock: string, size: number, active: boolean,
      stopping: boolean, positions: Array<Position>, profitOrLoss: number, ROI: number,
      windowsize: number, exitThreshold: number) {
    super("PB", ID, stock, size, active, stopping, positions, profitOrLoss, ROI);
    this.windowsize = windowsize;
    this.exitThreshold = exitThreshold;
  }
}
