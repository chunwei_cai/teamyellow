import { Position } from "./position";
import { Trader } from "./trader";

/**
 * Serializable/deserializable encapsulation of a BB strategy,
 * including its trading history, profitability, and ROI.
 *
 * @author Dan Li
 */
export class BollingerBands extends Trader {
  windowsize: number;
  multiplier: number;
  exitThreshold: number;

  constructor(ID: number, stock: string, size: number, active: boolean,
      stopping: boolean, positions: Array<Position>, profitOrLoss: number, ROI: number,
      windowsize: number, multiplier: number, exitThreshold: number) {
    super("BB", ID, stock, size, active, stopping, positions, profitOrLoss, ROI);
    this.windowsize = windowsize;
    this.multiplier = multiplier;
    this.exitThreshold = exitThreshold;
  }
}
