import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { TraderTable, NgbdModalContent } from './trader-table';

@NgModule({
  imports: [BrowserModule, NgbModule],
  declarations: [TraderTable, NgbdModalContent],
  exports: [TraderTable],
  bootstrap: [TraderTable],
  entryComponents: [NgbdModalContent]
})
export class NgbdModalComponentModule {}
