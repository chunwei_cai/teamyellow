import { Component, Input } from "@angular/core";
import { Trader } from "../model/trader";
import { TwoMovingAverages } from "../model/two-moving-averages";
import { TraderService } from "../model/trader-service";
import { TraderUpdate } from "../model/trader-service";
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Position } from "../model/position";
import { BollingerBands } from "../model/bollinger-bands";
import { PriceBreakers } from "../model/price-breakout";

/**
 * Angular component showing all current traders, including their
 * type, parameters, state, and profitability.
 *
 * @author Will Provost
 */
@Component({
  selector: "trader-table",
  templateUrl: "./trader-table.html",
  styleUrls: ["./trader-table.css"]
})
export class TraderTable implements TraderUpdate {


  service: TraderService;
  traders: Array<Trader> = [];
  traderState = new Map();
  currentTrader : Trader;
  allPositions;// : Array<Position> = [];
  name;
  windowsize;
  exitThreshold;

  /**
   * Helper to format times in mm:ss format.
   */
  minSec(millis: number): string {
    let seconds = millis / 1000;
    const minutes = Math.floor(seconds / 60);
    seconds = seconds % 60;
    const pad = seconds < 10 ? "0" : "";
    return "" + minutes + ":" + pad + seconds;
  }

  /**
   * Store the injected service references.
   */
  constructor(service: TraderService, private modalService: NgbModal) {
    this.service = service;
    service.subscribe(this);
    service.notify();
  }



  /**
   * Replace our array of traders with the latest,
   * determine the state of each trader out of 4 different states and triger a UI update
   */
  latestTraders(traders: Array<Trader>) {
    this.traders = traders;
    this.traders.forEach((trader) => {
      switch (this.traderState.get(trader.ID)) {
        case State.STARTING: {
          this.traderState.set(trader.ID, trader.active ? State.STARTED : State.STARTING);
          break;
        }
        case State.STOPPING: {
          this.traderState.set(trader.ID, trader.active ? State.STOPPING : State.STOPPED);
          console.log(trader.positions[0])
          break;
        }
        default: {
          this.traderState.set(trader.ID, trader.active ? State.STARTED : State.STOPPED);
          break;
        }
      }
    });
  }

  /**
   * Helper to derive a label for the trader's state: "Started",
   * "Stopped", or, if deactivated but still closing out a position,
   * "Stopping".
   */
  getState(trader: Trader) {
    //console.log(trader.ID, this.traderState.get(trader.ID), trader.active)
    return this.traderState.get(trader.ID);
  }

  /**
   * Helper to derive the total number of trades made by this trader.
   */
  getTotalTrades(): number {
    return this.traders.map(t => t.trades).reduce((x, y) => x + y, 0);
  }

  /**
   * Helper to derive the trader's total profit.
   */
  getTotalProfit(): number {
    return this.traders.map(t => t.profitOrLoss).reduce((x, y) => x + y, 0);
  }

  getAvgROI() : number {
    let totalROI = 0;
    let count = 0;
    // return totalROI / this.traders.length;

    for (let i = 0; i < this.traders.length; i++) {
      //console.log(this.traders[i].ROI);
      if (this.traders[i].trades > 1) {
        totalROI = totalROI + this.traders[i].ROI * 100.0;
        count = count + 1;
        console.log(totalROI);
      }
    }

    if (count == 0)
      return count;

    return totalROI / count;
  }

  /**
   * Finds the trader at the given table index and uses the
   * TraderService component to send an HTTP request to toggle the
   * trader's state.
   */
  startOrStop(ev: any, hardStop: boolean) {
    const index = ev.target.id.replace("trader", "");
    const trader = this.traders[index];
    this.traderState.set(trader.ID, trader.active ? State.STOPPING : State.STARTING)
    this.service.setActive(trader.ID, !trader.active);
  }

  moreInfo(ev: any){
    let id = Number(ev.target.value);
    console.log(id);

    this.service.getTrader(id).then(result => {
      // console.log('test',result.positions[0]);
       this.allPositions = result.positions;
       console.log(this.allPositions[0])
      // console.log("From result: " + this.allPositions[1].toString());


      const modalRef = this.modalService.open(NgbdModalContent, { size: 'xl' });
      modalRef.componentInstance.name = 'World';
      let posArr = [];
      for (let [key, value] of Object.entries(this.allPositions)) {
        modalRef.componentInstance.allPositions.push(value);
      }
      // console.log(posArr);
      // modalRef.componentInstance.allPositions = posArr;
    })

  }

}

enum State {
  STARTED = "Started",
  STOPPED = "Stopped",
  STOPPING = "Stopping",
  STARTING = "Starting"
}


@Component({
  selector: 'ngbd-modal-content',
  templateUrl: "./popup.html",
  styleUrls: ["./popup.css"]
})
export class NgbdModalContent {
  @Input() name;
  @Input() allPositions = [];

  constructor(public activeModal: NgbActiveModal) {

  }
}
