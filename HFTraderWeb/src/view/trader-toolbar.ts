import { Component } from "@angular/core";
import { Trader } from "../model/trader";
import { TwoMovingAverages } from "../model/two-moving-averages";
import { TraderService } from "../model/trader-service";
import { BollingerBands } from "../model/bollinger-bands";
import { PriceBreakers } from 'src/model/price-breakout';

/**
 * Angular component for a toolbar that allows the user to configure
 * and to create new traders. The component logic and the HTML template
 * currently support only the 2MA trader type.
 *
 * @author Will Provost
 */
@Component({
  selector: "trader-toolbar",
  templateUrl: "./trader-toolbar.html",
  styleUrls: ["./trader-toolbar.css"]
})
export class TraderToolbar {

  service: TraderService;
  type: string;
  stock: string;
  size: number;
  lengthShort: number;
  lengthLong: number;
  exitThreshold: number;
  windowsize: number;
  multiplier : number;

  /**
   * Set default values for all properties, which will flow out to the
   * initial UI via two-way binding.
   */
  constructor(service: TraderService) {
    this.service = service;

    this.type = "2MA";
    this.stock = "MRK";
    this.size = 1000;
    this.lengthShort = 30;
    this.lengthLong = 60;
    this.exitThreshold = 3;
    this.windowsize = 30;
    this.multiplier = 0.15;
  }

  /**
   * Reads the values of form controls via two-way binding.
   * Creates an instance of the trader (only 2MA traders currently supported)
   * and sends it to the server to be created and activated.
   */
  create() {
    console.log(this.type);
    if (this.type === "2MA"){
      this.service.createTrader(new TwoMovingAverages
        (0, this.stock, this.size, true, false, [], 0, NaN,
          this.lengthShort * 1000, this.lengthLong * 1000, this.exitThreshold / 100));
    }

    if (this.type === "BB") {
      let bbTrader = new BollingerBands(1, this.stock, this.size, true, false, [], 0, NaN,
        this.windowsize * 1000, this.multiplier, this.exitThreshold / 100);
      console.log(bbTrader);
      this.service.createTrader(bbTrader);
    }

    if (this.type === "PB") {
      let pbTrader = new PriceBreakers(1, this.stock, this.size, true, false, [], 0, NaN,
        this.windowsize * 1000, this.exitThreshold / 100);
      console.log(pbTrader);
      this.service.createTrader(pbTrader);
    }
  }
}
