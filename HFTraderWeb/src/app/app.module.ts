import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AlertModule } from 'ngx-bootstrap';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TraderTable, NgbdModalContent } from '../view/trader-table';
import { TraderToolbar } from '../view/trader-toolbar';

@NgModule({
  declarations: [
    AppComponent,
    TraderTable,
    TraderToolbar,
    NgbdModalContent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AlertModule.forRoot(),
    NgbModule
  ],
  providers: [],
  exports: [TraderTable],
  bootstrap: [AppComponent],
  entryComponents: [NgbdModalContent]
})
export class AppModule { }
