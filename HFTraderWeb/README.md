# HFTraderWeb

This Angular application provides the UI for the HFTrader application.
To set it up, do the following:

1. Install the latest NodeJS. Open a command shell with the root of your
   NodeJS installation as the working directory. Make sure that your
   NodeJS installation's root directory is in the executable path.

2. npm install -g @angular/cli

3. Create a folder under your Node installation called 'HFTraderWeb',
   and make it your working directory.

4. git clone this repository.

5. npm install

6. ng serve, ng test, etc.

The application is set up to assume a development environment in which
the HFTrader URL is http://localhost:8082. If different on your system
(and eventually as part of cloud deployment) change this setting in
src/model/trader-service.ts.

