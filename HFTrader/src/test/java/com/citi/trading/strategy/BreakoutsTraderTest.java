package com.citi.trading.strategy;

import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

import java.sql.Timestamp;
import java.util.function.Consumer;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.OrderPlacer;
import com.citi.trading.Trade;
import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.PricingSource;
import com.citi.trading.strategy.TraderTest.TraderMocks;

/**
 * Unit test for the {@link BreakoutsTrader}. We configure mock
 * pricing, market, and persistence, and aggressively verify outbound calls
 * as a way to check the trading behavior of the component.
 * 
 * @author Jingyu Wang
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=BreakoutsTraderTest.Config.class)

@DirtiesContext(classMode=ClassMode.AFTER_EACH_TEST_METHOD)
public class BreakoutsTraderTest {

	public static final String STOCK = "AA";
	public static final int SIZE = 100;
	public static final double PRICE = 100.0;

	@Configuration
	@Import(TraderMocks.class)
	public static class Config {
		
		@Bean
		public BreakoutsTrader littleBreakoutsTrader(PricingSource pricing, 
				OrderPlacer market, StrategyPersistence strategyPersistence) {
			Breakouts Br = 
					new Breakouts(STOCK,SIZE, 30000, 0.03);
			BreakoutsTrader trader = 
					new BreakoutsTrader(pricing, market, strategyPersistence); 
			trader.setStrategy(Br);
			return trader;
		}

	}
	
	@Resource(name="littleBreakoutsTrader")
	private BreakoutsTrader littleTrader;
	
	@Autowired
	private OrderPlacer mockMarket;
	
	private Trade latestTrade;
	private Consumer<Trade> latestSubscriber;
	
	/**
	 * Automatically and immediately confirm any requested trade in full.
	 */
	@Before
	@SuppressWarnings("unchecked") // Consumer<Trade> from Object
	public void setUp() {
		doAnswer(inv -> {
				latestTrade = (Trade) inv.getArgument(0);
				latestTrade.setResult(Trade.Result.FILLED);
				latestSubscriber = (Consumer<Trade>) inv.getArgument(1);
				return null;
			}).when(mockMarket).placeOrder(any(), any());		
	}
	
	public static void addPricePoint(PriceData data, double price) {
		Timestamp time = data.getLatestTimestamp();
		PricePoint point = new PricePoint(new Timestamp(time.getTime() + 15000), 
				price, price, price, price, 100);
		data.addData(point);
	}
	
	public static void addDetailedPricePoint(PriceData data, double open, double high, double low, double close) {
		Timestamp time = data.getLatestTimestamp();
		PricePoint point = new PricePoint(new Timestamp(time.getTime() + 15000), 
				open, high, low, close, 100);
		data.addData(point);
	}
	
	public static PriceData createPriceData(double... prices) {
		PricePoint[] points = new PricePoint[prices.length];
		Timestamp time = new Timestamp(1546351200000L); // Jan 1 2019, 9:00 AM
		int index = 0;
		for (double price : prices) {
			points[index++] = new PricePoint(time, price, price, price, price, 100);
			time = new Timestamp(time.getTime() + 15000);
		}
		PriceData data = new PriceData("", prices.length);
		data.addData(points);
		return data;
	}
	
	public static PriceData createDetailedPriceData() {
		PricePoint[] points = new PricePoint[2];
		Timestamp time = new Timestamp(1546351200000L); // Jan 1 2019, 9:00 AM

			points[0] = new PricePoint(time, 109, 110, 90, 91, 100);
			points[1] = new PricePoint(time, 107, 108, 92, 93, 100);
			time = new Timestamp(time.getTime() + 15000);

		PriceData data = new PriceData("", 2);
		data.addData(points);
		return data;
	}
	
	private void confirmAndRecordRequestedTrade(BreakoutsTrader trader) {
		latestSubscriber.accept(latestTrade);
		if (trader.isOpen()) {
			trader.getStrategy().getOpenPosition().setClosingTrade(latestTrade);
		} else {
			Position position = new Position(trader.getStrategy(), latestTrade);
			trader.getStrategy().addPosition(position);
		}
	}


	@Test
	public void testNoTrend() {
		PriceData data = createPriceData(100, 100);
		littleTrader.accept(data);
		verify(mockMarket, never()).placeOrder(any(), any());
		
		addPricePoint(data, 99);
		littleTrader.accept(data);
		verify(mockMarket, never()).placeOrder(any(), any());
	}

	@Test
	public void testSellsAndThenCloseAndNoTrendAndThenBuys() {
		PriceData data = createDetailedPriceData();
		littleTrader.accept(data);
		verify(mockMarket, never()).placeOrder(any(), any());
		
		addDetailedPricePoint(data, 98, 100, 90, 91);
		littleTrader.accept(data);
		verify(mockMarket).placeOrder((Trade) argThat(both(hasProperty("buy", equalTo(false)))
		.and(hasProperty("price", closeTo(91.0, 0.0001)))), any());
	
		confirmAndRecordRequestedTrade(littleTrader);
		
		addDetailedPricePoint(data, 98, 110, 90, 91);
		littleTrader.accept(data);
		addDetailedPricePoint(data, 96, 97, 92, 93);
		littleTrader.accept(data);
		addDetailedPricePoint(data, 91, 100, 90, 98);
		littleTrader.accept(data);
		
		verify(mockMarket).placeOrder((Trade) argThat(both(hasProperty("buy", equalTo(true)))
		.and(hasProperty("price", closeTo(98.0, 0.0001)))), any());
	}
	
	@Test
	public void testTrendAndOpenCloseEqual() {
		PriceData data = createDetailedPriceData();
		littleTrader.accept(data);
		verify(mockMarket, never()).placeOrder(any(), any());
		
		addDetailedPricePoint(data, 94, 94, 94, 94);
		littleTrader.accept(data);
		verify(mockMarket).placeOrder((Trade) argThat(both(hasProperty("buy", equalTo(false)))
		.and(hasProperty("price", closeTo(94.0, 0.0001)))), any());

	}
}
