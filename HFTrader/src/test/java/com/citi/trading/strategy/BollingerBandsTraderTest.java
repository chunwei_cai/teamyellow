package com.citi.trading.strategy;

import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

import java.sql.Timestamp;
import java.util.function.Consumer;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.OrderPlacer;
import com.citi.trading.Trade;
import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.PricingSource;
import com.citi.trading.strategy.TraderTest.TraderMocks;

/**
 * Unit test for the {@link BollingerBandsTrader}. We configure mock
 * pricing, market, and persistence, and aggressively verify outbound calls
 * as a way to check the trading behavior of the component.
 * 
 * @author Jingyu Wang
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=BollingerBandsTraderTest.Config.class)

@DirtiesContext(classMode=ClassMode.AFTER_EACH_TEST_METHOD)
public class BollingerBandsTraderTest {

	public static final String STOCK = "AA";
	public static final int SIZE = 100;
	public static final double PRICE = 100.0;

	@Configuration
	@Import(TraderMocks.class)
	public static class Config {
		
		@Bean
		public BollingerBandsTrader littleBBTrader(PricingSource pricing, 
				OrderPlacer market, StrategyPersistence strategyPersistence) {
			BollingerBands BB = 
					new BollingerBands(STOCK,SIZE, 60000, 0.3, 0.03);
			BollingerBandsTrader trader = 
					new BollingerBandsTrader(pricing, market, strategyPersistence); 
			trader.setStrategy(BB);
			return trader;
		}
	}
	
	@Resource(name="littleBBTrader")
	private BollingerBandsTrader littleTrader;
	
	@Autowired
	private OrderPlacer mockMarket;
	
	private Trade latestTrade;
	private Consumer<Trade> latestSubscriber;
	
	/**
	 * Automatically and immediately confirm any requested trade in full.
	 */
	@Before
	@SuppressWarnings("unchecked") // Consumer<Trade> from Object
	public void setUp() {
		doAnswer(inv -> {
				latestTrade = (Trade) inv.getArgument(0);
				latestTrade.setResult(Trade.Result.FILLED);
				latestSubscriber = (Consumer<Trade>) inv.getArgument(1);
				return null;
			}).when(mockMarket).placeOrder(any(), any());		
	}
	
	public static void addPricePoint(PriceData data, double price) {
		Timestamp time = data.getLatestTimestamp();
		PricePoint point = new PricePoint(new Timestamp(time.getTime() + 15000), 
				price, price, price, price, 100);
		data.addData(point);
	}
	
	public static PriceData createPriceData(double... prices) {
		PricePoint[] points = new PricePoint[prices.length];
		Timestamp time = new Timestamp(1546351200000L); // Jan 1 2019, 9:00 AM
		int index = 0;
		for (double price : prices) {
			points[index++] = new PricePoint(time, price, price, price, price, 100);
			time = new Timestamp(time.getTime() + 15000);
		}
		PriceData data = new PriceData("", prices.length);
		data.addData(points);
		return data;
	}


	@Test
	public void testNoTradeAndThenBuys() {
		PriceData data = createPriceData(100, 100, 100, 100);
		littleTrader.accept(data);
		verify(mockMarket, never()).placeOrder(any(), any());
		
		addPricePoint(data, 99);
		littleTrader.accept(data);
		verify(mockMarket).placeOrder((Trade) argThat(both(hasProperty("buy", equalTo(true)))
				.and(hasProperty("price", closeTo(99.0, 0.0001)))), any());
	}

	@Test
	public void testNoTadesAnThenSells() {
		PriceData data = createPriceData(100, 100, 100, 100);
		littleTrader.accept(data);
		verify(mockMarket, never()).placeOrder(any(), any());
		
		addPricePoint(data, 100);
		littleTrader.accept(data);
		verify(mockMarket, never()).placeOrder(any(), any());
		
		addPricePoint(data, 101);
		littleTrader.accept(data);
		verify(mockMarket).placeOrder((Trade) argThat(both(hasProperty("buy", equalTo(false)))
		.and(hasProperty("price", closeTo(101.0, 0.0001)))), any());

	}

}
