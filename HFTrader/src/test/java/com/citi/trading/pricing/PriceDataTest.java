package com.citi.trading.pricing;

import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.Pricing;

/**
 * Unit test for the {@link PriceData} component. Test cases cover a handful
 * of common situations: exactly enough data to fill the capacity, not enough,
 * or more so that some data is overwritten. Assure that the component 
 * correctly manages the complexities of the circular array, reports
 * the right data as the most recent, and calculates averages correctly.
 * 
 * @author Will Provost
 */
public class PriceDataTest {

	public static final PricePoint POINT1 = Pricing.parsePricePoint
			("2019-05-31 13:15:30.620,100.0000,110.0000,90.0000,100.0000,10000");
	public static final PricePoint POINT2 = Pricing.parsePricePoint
			("2019-05-31 13:15:30.620,100.0000,110.0000,90.0000,105.0000,20000");
	public static final PricePoint POINT3 = Pricing.parsePricePoint
			("2019-05-31 13:15:30.620,105.0000,110.0000,90.0000,110.0000,30000");
	public static final PricePoint POINT4 = Pricing.parsePricePoint
			("2019-05-31 13:15:30.620,110.0000,120.0000,100.0000,115.0000,40000");
	public static final PricePoint POINT5 = Pricing.parsePricePoint
			("2019-05-31 13:15:30.620,115.0000,125.0000,110.0000,120.0000,50000");
	public static final PricePoint POINT6 = Pricing.parsePricePoint
			("2019-05-31 13:15:30.620,120.0000,125.0000,115.0000,120.0000,60000");
	
	// points for checkTrend, close bigger
	public static final PricePoint POINT7 = Pricing.parsePricePoint
			("2019-05-31 13:15:30.620,100.0000,110.0000,100.0000,110.0000,10000");
	public static final PricePoint POINT8 = Pricing.parsePricePoint
			("2019-05-31 13:15:30.620,101.0000,109.0000,101.2000,109.0000,20000");
	public static final PricePoint POINT9 = Pricing.parsePricePoint
			("2019-05-31 13:15:30.620,103.0000,108.0000,102.0000,108.1000,30000");
	
	// points for checkTrend, close open flip
	public static final PricePoint POINT10 = Pricing.parsePricePoint
			("2019-05-31 13:15:30.620,100.0000,110.0000,100.0000,110.0000,10000");
	public static final PricePoint POINT11 = Pricing.parsePricePoint
			("2019-05-31 13:15:30.620,101.0000,109.0000,101.2000,109.0000,20000");
	public static final PricePoint POINT12 = Pricing.parsePricePoint
			("2019-05-31 13:15:30.620,108.0000,108.0000,102.0000,103.1000,30000");
	
	/////////////////////////////////////////////////////////////////////////////
	// Tests where the buffer is exactly full.
	
	public PriceData createFilledPriceData() {
		PriceData priceData = new PriceData("XXX", 6);
		priceData.addData(POINT1, POINT2, POINT3, POINT4, POINT5, POINT6);
		return priceData;
	}
	
	@Test
	public void testFilledAll() {
		PriceData priceData = createFilledPriceData();
		assertThat(priceData.getData(3).count(), equalTo(3L));
		assertThat(priceData.getCapacity(), equalTo(6));
		assertThat(priceData.getSize(), equalTo(6));
		assertThat(priceData.getWindowAverage(6, PricePoint::getVolume), 
				closeTo(35000, .01));
		assertThat(priceData.getWindowAverage(3, PricePoint::getVolume), 
				closeTo(50000, .01));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testFilledAllAskForTooMuch() {
		PriceData priceData = createFilledPriceData();
		priceData.getData(8);
	}
	
	@Test
	public void testFilledAllAskForNothing() {
		PriceData priceData = createFilledPriceData();
		assertThat(priceData.getData(0).count(), equalTo(0L));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testFilledAllAskForAverageOfNothing() {
		PriceData priceData = createFilledPriceData();
		priceData.getWindowAverage(0, null);
	}

	/////////////////////////////////////////////////////////////////////////////
	// Tests where the buffer has been filled and some of it overwritten.
	
	// Points 1 and 2 are gone; circular array holds 5, 6, 3, 4
	public PriceData createOverfilledPriceData() {
		PriceData priceData = new PriceData("XXX", 4);
		priceData.addData(POINT1, POINT2, POINT3, POINT4, POINT5, POINT6);
		return priceData;
	}
	
	@Test
	public void testOverfilledAll() {
		PriceData priceData = createOverfilledPriceData();
		assertThat(priceData.getData(3).count(), equalTo(3L));
		assertThat(priceData.getCapacity(), equalTo(4));
		assertThat(priceData.getSize(), equalTo(4));
		assertThat(priceData.getWindowAverage(2, PricePoint::getVolume), 
				closeTo(55000, .01));
		assertThat(priceData.getWindowAverage(3, PricePoint::getVolume), 
				closeTo(50000, .01));
	}
	
	@Test
	public void testOverfilledThenExpand() {
		PriceData priceData = createOverfilledPriceData();
		priceData.expandTo(10);
		assertThat(priceData.getCapacity(), equalTo(10));
		assertThat(priceData.getSize(), equalTo(0));
	}

	@Test(expected=IllegalArgumentException.class)
	public void testOverfilledThenExpandNegatively() {
		PriceData priceData = createOverfilledPriceData();
		priceData.expandTo(2);
	}
	
	/////////////////////////////////////////////////////////////////////////////
	// Tests where the buffer has not been filled yet.
	
	public PriceData createUnfilledPriceData() {
		PriceData priceData = new PriceData("XXX", 8);
		priceData.addData(POINT1, POINT2, POINT3, POINT4, POINT5, POINT6);
		return priceData;
	}

	@Test
	public void testUnfilledAll() {
		PriceData priceData = createUnfilledPriceData();
		assertThat(priceData.getData(3).count(), equalTo(3L));
		assertThat(priceData.getCapacity(), equalTo(8));
		assertThat(priceData.getSize(), equalTo(6));
		assertThat(priceData.getWindowAverage(6, PricePoint::getVolume), 
				closeTo(35000, .01));
		assertThat(priceData.getWindowAverage(3, PricePoint::getVolume), 
				closeTo(50000, .01));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testUnfilledAllAskForTooMuch() {
		PriceData priceData = createUnfilledPriceData();
		priceData.getData(7);
	}

	/////////////////////////////////////////////////////////////////////////////
	// Tests of the addData() method
	
	@Test
	public void testAddDataNotRepeatable() {
		PriceData priceData = new PriceData("XXX", 4);
		assertThat(priceData.addData(POINT1), equalTo(true));
		assertThat(priceData.addData(POINT1), equalTo(false));
	}
	
	@Test
	public void testAddDataNoGoingBack() {
		PriceData priceData = new PriceData("XXX", 4);
		assertThat(priceData.addData(POINT2), equalTo(true));
		assertThat(priceData.addData(POINT1), equalTo(false));
	}
	
	//////////////////////////////////////////////////////////////////////////////////
	//Tests of the checkTrend method
	
	// Close prices are always greater than open prices
	public PriceData createTrendedPriceData1() {
		PriceData priceData = new PriceData("XXX", 3);
		priceData.addData(POINT7, POINT8, POINT9);
		return priceData;
	}
	
	// some times open > close sometimes close > open
	public PriceData createTrendedPriceData2() {
		PriceData priceData = new PriceData("XXX", 3);
		priceData.addData(POINT10, POINT11, POINT12);
		return priceData;
	}
	
	// there is trend, with price points that close > open all the time
	@Test
	public void testCheckTrendCloseBigger() {
		PriceData priceData = createTrendedPriceData1();
		assertThat(priceData.checkTrend(3), equalTo(true));
	}
	
	// there is trend, with price points that sometimes open > close sometimes close > open
	@Test
	public void testCheckTrendOpenCloseFlipped() {
		PriceData priceData = createTrendedPriceData2();
		assertThat(priceData.checkTrend(3), equalTo(true));
	}
	
	@Test
	public void testCheckNoTrend() {
		PriceData priceData = createFilledPriceData();
		assertThat(priceData.checkTrend(6), equalTo(false));

	}
}


