package com.citi.trading.strategy;

import static com.citi.trading.pricing.Pricing.SECONDS_PER_PERIOD;

import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.citi.trading.OrderPlacer;
import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.PricingSource;

/**
 * Implementation of the Breakouts trading strategy.
 *
 * @author Jingyu Wang
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BreakoutsTrader extends Trader<Breakouts> {
	
    private static final Logger LOGGER =
        Logger.getLogger(BreakoutsTrader.class.getName ());
    
    private boolean trendFound = false;
    private boolean closeHigher = false;
    
    public BreakoutsTrader(PricingSource pricing, 
    		OrderPlacer market, StrategyPersistence strategyPersistence) {
    	super(pricing, market, strategyPersistence);
    }
    
    protected int periods(int msec) {
    	return msec / 1000 / SECONDS_PER_PERIOD;
    }
    
    public int getNumberOfPeriodsToWatch() {
    	return periods(strategy.getWindowsize());
    }
    
    /**
     * Helper method to update our flags: Is the current price lower than the lower band
     * or greater than the upper band, or within?
     */
    private boolean checkBreakouts(PriceData data) {
        final double TOLERANCE = 0.0001;
        
        int numberOfPoints=periods(strategy.getWindowsize());
        boolean breakoutFound = false;
        if (!trendFound) { // if no shrinking trend found, look for it
            trendFound = data.checkTrend(numberOfPoints);
        }else {// if shrinking trend found, look for breakouts
        	// compare current price point's high and low 
        	// with previous price point's open and close to identify breakouts
        	LOGGER.info("Trader " + strategy.getId() + " trend found."+trendFound);
        	Stream<PricePoint> currentPoints = data.getData(2);
        	Iterator<PricePoint> iterator = currentPoints.iterator();
        	PricePoint previous = iterator.next();
    		PricePoint current = iterator.next();
    		
			double previousOpen = previous.getOpen();
			double previousClose = previous.getClose();
			double currentHigh = current.getHigh();
			double currentLow = current.getLow();
			LOGGER.info("Trader " + strategy.getId() + " previous point."+previous);
			LOGGER.info("Trader " + strategy.getId() + " current point."+current);
			boolean topShrink = currentHigh < Math.max(previousOpen, previousClose);
			boolean bottomShrink = currentLow > Math.min(previousOpen, previousClose);
			// edge case: if no breakout but high and low are equal, make a trade, too
			boolean highLowEqual = (currentHigh-currentLow)/currentHigh < TOLERANCE;

			if (!bottomShrink || !topShrink || highLowEqual) {
				breakoutFound = true;
				double currentOpen = current.getOpen();
				double currentClose = current.getClose();
				closeHigher = currentClose > currentOpen;
				trendFound = false; // reset trend found flag as breakout found
			}
        }
		return breakoutFound;
    }
    
    /**
     * When we're open, just check to see if the latest closing price is a profit or
     * loss greater than our configured threshold. If it is, use the base class'
     * <strong>Closer</strong> to close the position.
     */
    protected void handleDataWhenOpen(PriceData data) { 
    	double currentPrice = data.getData(1).findAny().get().getClose();
    	checkBreakouts(data);
    	double openingPrice = getStrategy().getOpenPosition().getOpeningTrade().getPrice();
    	double profitOrLoss = currentPrice / openingPrice - 1.0;
    	if (Math.abs(profitOrLoss) > strategy.getExitThreshold()) {
    		closer.placeOrder(currentPrice);
    		if (!getStrategy().getOpenPosition().getOpeningTrade().isBuy()) {
    			profitOrLoss = 0 - profitOrLoss;
    		}
    		LOGGER.info(String.format("Trader " + strategy.getId() + " closing position on a profit/loss of %5.3f percent.", 
    				profitOrLoss * 100));
    	}
    } 
    
    /**
     * When we're closed, check if there is trend and then breakout.
     * If there is trend and then breakout:
     * If current close is greater than current open, buy to open a long position;
     * if current close is lower than current open, sell to open a short position.
     */
    protected void handleDataWhenClosed(PriceData data) {
    	if (tracking.get()) {    	
    		boolean breakoutFound = checkBreakouts(data);
    		

    		LOGGER.info("Trader " + strategy.getId() + " breakoutfound."+breakoutFound);
    		if (breakoutFound) {
    			boolean isBuy = false;
    			if (closeHigher) {
    				isBuy = true;
    			}
    			opener.placeOrder(isBuy, data.getData(1).findAny().get().getClose());
    			LOGGER.info("Trader " + strategy.getId() + " opening position; isbuy? " + isBuy);
    		}
    		
	    } else if (data.getSize() >= getNumberOfPeriodsToWatch()) {
	    	checkBreakouts(data);
	    	tracking.set(true);
	    	LOGGER.info("Trader " + strategy.getId() + " got initial pricing data and baseline averages.");
	    }
    }
}
