package com.citi.trading.strategy;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


/**
 * Represents a Breakouts trading strategy, adding the parameters
 * that are specific to that algorithm.
 * 
 * @author Jingyu Wang
 */
@Entity
@DiscriminatorValue("C")
public class Breakouts extends Strategy implements Serializable {
	private static final long serialVersionUID = 1L;

	private int windowsize;
	private double exitThreshold;

	public Breakouts() {
	}
	
	public Breakouts(String stock, int size, int windowsize, double exitThreshold) {
		super(stock, size);
		this.windowsize = windowsize;
		this.exitThreshold = exitThreshold;
	}

	public double getExitThreshold() {
		return this.exitThreshold;
	}

	public void setExitThreshold(double exitThreshold) {
		this.exitThreshold = exitThreshold;
	}

	public int getWindowsize() {
		return this.windowsize;
	}

	public void setWindowsize(int windowsize) {
		this.windowsize= windowsize;
	}



	@Override
	public String toString() {
		return String.format("Breakouts: [windowsize=%d, exit=%1.4f, %s]",
				windowsize, exitThreshold, stringRepresentation());
	}
}
