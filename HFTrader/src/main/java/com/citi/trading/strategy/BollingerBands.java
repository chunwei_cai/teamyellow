package com.citi.trading.strategy;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


/**
 * Represents a BollingerBands trading strategy, adding the parameters
 * that are specific to that algorithm.
 * 
 * @author Jingyu Wang
 */
@Entity
@DiscriminatorValue("B")
public class BollingerBands extends Strategy implements Serializable {
	private static final long serialVersionUID = 1L;

	private int windowsize;
	private double exitThreshold;
	private double multiplier;

	public BollingerBands() {
	}
	
	public BollingerBands(String stock, int size, int windowsize, double exitThreshold, double multiplier) {
		super(stock, size);
		
		this.windowsize = windowsize;
		this.exitThreshold = exitThreshold;
		this.multiplier = multiplier;
	}

	public double getExitThreshold() {
		return this.exitThreshold;
	}

	public void setExitThreshold(double exitThreshold) {
		this.exitThreshold = exitThreshold;
	}

	public int getWindowsize() {
		return this.windowsize;
	}

	public void setWindowsize(int windowsize) {
		this.windowsize= windowsize;
	}
	
	public double getMultiplier() {
		return this.multiplier;
	}

	public void setMultiplier(double multiplier) {
		this.multiplier= multiplier;
	}

	@Override
	public String toString() {
		return String.format("BollingerBands: [windowsize=%d, exit=%1.4f, multiplier=%1.4f, %s]", 
				windowsize, exitThreshold, multiplier, stringRepresentation());
	}
}
