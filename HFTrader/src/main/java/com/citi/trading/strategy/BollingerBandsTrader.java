package com.citi.trading.strategy;

import static com.citi.trading.pricing.Pricing.SECONDS_PER_PERIOD;

import java.util.logging.Logger;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.citi.trading.OrderPlacer;
import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.PricingSource;

/**
 * Implementation of the bollinger bands trading strategy.
 *
 * @author Jingyu Wang
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BollingerBandsTrader extends Trader<BollingerBands> {
	
    private static final Logger LOGGER =
        Logger.getLogger(BollingerBandsTrader.class.getName ());
    
    private boolean inBetween = true;
    private boolean lower;
    private boolean higher;
    
    public BollingerBandsTrader(PricingSource pricing, 
    		OrderPlacer market, StrategyPersistence strategyPersistence) {
    	super(pricing, market, strategyPersistence);
    }
    
    protected int periods(int msec) {
    	return msec / 1000 / SECONDS_PER_PERIOD;
    }
    
    public int getNumberOfPeriodsToWatch() {
    	return periods(strategy.getWindowsize());
    }
    
    /**
     * Helper method to update our flags: Is the current price lower than the lower band
     * or greater than the upper band, or within?
     */
    private void checkBands(PriceData data) {
		double currentPrice = data.getData(1).findAny().get().getClose();

        final double TOLERANCE = 0.0001;
        int numberOfPoints=periods(strategy.getWindowsize());
        double movingAverage = data.getWindowAverage
    			(numberOfPoints, PricePoint::getClose);
        double std = data.getWindowStd
    			(numberOfPoints, PricePoint::getClose);
        double upperBand = movingAverage + std * strategy.getMultiplier();
        double lowerBand = movingAverage - std * strategy.getMultiplier();
        // exactly hits low/high
    	boolean checkLowHits = Math.abs(lowerBand - currentPrice) / lowerBand < TOLERANCE;
    	boolean checkHighHits = Math.abs(upperBand - currentPrice) / upperBand < TOLERANCE;
    			
    	lower = !checkLowHits && (currentPrice < lowerBand);
    	higher = !checkHighHits && (currentPrice > upperBand);
    	inBetween = !lower && !higher;
    }
    
    /**
     * When we're open, just check to see if the latest closing price is a profit or
     * loss greater than our configured threshold. If it is, use the base class'
     * <strong>Closer</strong> to close the position.
     */
    protected void handleDataWhenOpen(PriceData data) { 
    	double currentPrice = data.getData(1).findAny().get().getClose();
    	checkBands(data);
    	double openingPrice = getStrategy().getOpenPosition().getOpeningTrade().getPrice();
    	double profitOrLoss = currentPrice / openingPrice - 1.0;
    	if (Math.abs(profitOrLoss) > strategy.getExitThreshold()) {
    		closer.placeOrder(currentPrice);
    		if (!getStrategy().getOpenPosition().getOpeningTrade().isBuy()) {
    			profitOrLoss = 0 - profitOrLoss;
    		}
    		LOGGER.info(String.format("Trader " + strategy.getId() + " closing position on a profit/loss of %5.3f percent.", 
    				profitOrLoss * 100));
    	}
    }
    
    /**
     * When we're closed, capture the most recent inBetween, lower, higher flags,
     * If below lower band, buy to open a long position;
     * if beyond upper band, sell to open a short position.
     */
    protected void handleDataWhenClosed(PriceData data) {
    	if (tracking.get()) {    	
    		
    		checkBands(data);
    		if (!inBetween) {
    			boolean isBuy = false;
    			if (lower) {
    				isBuy = true;
    			}
    			if (higher) {
    				isBuy = false;
    			}
    			opener.placeOrder(isBuy, data.getData(1).findAny().get().getClose());
    			LOGGER.info("Trader " + strategy.getId() + " opening position; isbuy? " + isBuy);
    		}
    		
	    } else if (data.getSize() >= getNumberOfPeriodsToWatch()) {
	    	checkBands(data);
	    	tracking.set(true);
	    	LOGGER.info("Trader " + strategy.getId() + " got initial pricing data and baseline averages.");
	    }
    }
}
