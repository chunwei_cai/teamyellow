(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\r\n<!-- <div style=\"background-color:rgba(255, 255, 255, 0.25); padding: 5px;\">\r\n<div class=\"container\" >\r\n        <div class=\"row\">\r\n                <div class=\"col-2\">\r\n                </div>\r\n                <div class=\"col-7\">\r\n                    <h1 id = \"titleName\" class = \"titleText\">\r\n                        <span style=\"color: #FD0000\">A</span>\r\n                        <span style=\"color: #003B6F\">utomated  </span>\r\n                        <span style=\"color: #FD0000\">T</span>\r\n                        <span style=\"color: #003B6F\">rading  </span>\r\n                        <span style=\"color: #FD0000\">P</span>\r\n                        <span style=\"color: #003B6F\">latform</span>\r\n                    </h1>\r\n                </div>\r\n                <div class=\"col-2\">\r\n                    <img src=\"../assets/citi.png\" height=\"60px\" class = \"float-right\">\r\n                </div>\r\n                <br>\r\n                <hr>\r\n        </div>\r\n</div>\r\n\r\n</div> -->\r\n\r\n<div class=\"container1\">\r\n    <img id = \"topImage\" src=\"../assets/top.jpg\">\r\n    <a href=\"#theToolbar\"><button class=\"btn\">Get Started</button></a>\r\n</div>\r\n\r\n<br>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<trader-toolbar></trader-toolbar>\r\n<br>\r\n<!-- <hr width=\"100%\" /> -->\r\n<trader-table></trader-table>\r\n\r\n<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/view/popup.html":
/*!*******************************************************!*\
  !*** ./node_modules/raw-loader!./src/view/popup.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n    <div class=\"modal-header\">\r\n        <h4 class=\"modal-title\">Trading History</h4>\r\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"activeModal.dismiss('Cross click')\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n        <!-- <h1>{{allPositions.id}}</h1><br> -->\r\n        <!-- <h1>{{allPositions | json}}</h1><br>\r\n        <p>Hello, {{name}}!</p> -->\r\n\r\n        <table class=\"table\">\r\n                <thead class=\"table-primary\">\r\n                        <th class=\"right\" >Stock</th>\r\n                        <th class=\"right\" >Open Date</th>\r\n                        <th class=\"right\" >Open Price</th>\r\n                        <th class=\"right\" >Open type</th>\r\n                        <th class=\"right\" >Close Date</th>\r\n                        <th class=\"right\" >Close Price</th>\r\n                        <th class=\"right\" >Close Type</th>\r\n                        <th class=\"right\" >Profit</th>\r\n                        <th class=\"right\" >ROI</th>\r\n                        <th class=\"right\" >Status</th>\r\n                </thead>\r\n\r\n                <tbody class = \"table table-striped\">\r\n                    <tr *ngFor='let position of allPositions;'>\r\n                        <td>{{position.openingTrade.stock | json}}</td>\r\n                        <td>{{position.openingTrade.when | date:'M/d/yy h:mma'}}</td>\r\n                        <td>{{position.openingTrade.price | currency}}</td>\r\n                        <!-- <td>{{position.openingTrade.buy}}</td> -->\r\n                        <!-- <td *ngIf = \"position; else openNotNull\">N/A</td> -->\r\n                        <td *ngIf = \"position.openingTrade != null && position.openingTrade.buy; else openSell\">Buy</td>\r\n                        <ng-template #openSell><td>Sell</td></ng-template>\r\n\r\n                        <td *ngIf = \"position.closingTrade != null; else noCloseWhen\">{{position.closingTrade.when | date:'M/d/yy h:mma'}}</td>\r\n                        <ng-template #noCloseWhen><td>N/A</td></ng-template>\r\n                        <td *ngIf = \"position.closingTrade != null; else noClosePrice\">{{position.closingTrade.price | currency}}</td>\r\n                        <ng-template #noClosePrice><td>N/A</td></ng-template>\r\n                        <td *ngIf = \"position.closingTrade != null && position.closingTrade.buy; else closeSell\">Buy</td>\r\n                        <ng-template #closeSell>\r\n                            <td *ngIf = \"position.closingTrade != null; else sellNotApply\">Sell</td>\r\n                            <ng-template #sellNotApply><td>N/A</td></ng-template>\r\n                        </ng-template>\r\n                        <td>${{position.profitOrLoss | number: '.1-1' }}</td>\r\n                        <td *ngIf = \"position.roi != 'NaN'; else nan\">{{ position.roi * 100 | number: '2.1-1'}}%</td>\r\n                        <ng-template #nan><td>N/A</td></ng-template>\r\n                        <td *ngIf = \"!position.open; else posOpen\">Close</td>\r\n                        <ng-template #posOpen><td>Opening</td></ng-template>\r\n                    </tr>\r\n                    \r\n                        <!-- <tr>{{position.openingTrade.when | json}}</tr> -->\r\n                        <!-- <tr class=\"right\">{{position}}</tr> -->\r\n                        <!-- <tr>{{name}}</tr> -->\r\n                        <!-- <tr>{{position | json}}</tr> -->\r\n                        <!-- <tr>{{allPositions.openingTrade.stock | json}}</tr> -->\r\n                </tbody>\r\n\r\n        </table>\r\n        \r\n\r\n    </div>\r\n    <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-outline-dark\" (click)=\"activeModal.close('Close click')\">Close</button>\r\n    </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/view/trader-table.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/view/trader-table.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n    <div class=\"row\">\r\n        <div class=\"col-3\" style=\"padding: 5px\">\r\n            <div class=\"card text-center\" style=\"width: 18rem;\">\r\n                <div class=\"card-body\">\r\n                    <h5 class=\"card-title\" >Strategy</h5>\r\n                        <p class=\"card-text\" style=\"font-size: 40px; color: #007BFF\">{{traders.length}}</p>\r\n                  </div>\r\n              </div>\r\n          </div>\r\n        <div class=\"col-3\" style=\"padding: 5px\">\r\n            <div class=\"card text-center\" style=\"width: 18rem;\">\r\n                <div class=\"card-body\">\r\n                    <h5 class=\"card-title\" >Total Trade</h5>\r\n                        <p *ngIf = \"getTotalTrades() != null; else noTradeTop\" class=\"card-text\" style=\"font-size: 40px; color: #007BFF\">{{getTotalTrades()}}</p>\r\n                        <ng-template #noTradeTop>\r\n                            <p class=\"card-text\" style=\"font-size: 40px; color: #007BFF\">0</p>\r\n                        </ng-template>\r\n                  </div>\r\n              </div>\r\n        </div>\r\n        <div class=\"col-3\" style=\"padding: 5px\">\r\n            <div class=\"card text-center\" style=\"width: 18rem;\">\r\n                <div class=\"card-body\">\r\n                    <h5 class=\"card-title\" >Total Profit</h5>\r\n                        <p class=\"card-text\" style=\"font-size: 40px; color: #007BFF\">{{getTotalProfit() | currency:'USD':'symbol':'1.0-0'}}</p>\r\n                  </div>\r\n              </div>\r\n        </div>\r\n        <div class=\"col-3\" style=\"padding: 5px\">\r\n            <div class=\"card text-center\" style=\"width: 18rem;\">\r\n                <div class=\"card-body\">\r\n                    <h5 class=\"card-title\" >Average ROI</h5>\r\n                        <p class=\"card-text\" style=\"font-size: 40px; color: #007BFF\">{{getAvgROI() | number: '1.2-2'}}%</p>\r\n                  </div>\r\n              </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<br>\r\n\r\n<!-- <h2>{{traders[6] | json}}</h2> -->\r\n\r\n<table id = \"tableBottom\" class=\"table\">\r\n  <thead class=\"table-primary\">\r\n    <tr>\r\n      <th>Stock</th>\r\n      <th class=\"right\" >Size</th>\r\n      <th class=\"right\" >Strategy</th>\r\n      <th class=\"right\" >Parameters</th>\r\n      <th class=\"right\" >Exit</th>\r\n      <th>State</th>\r\n      <th class=\"right\" >Trades</th>\r\n      <th class=\"right\" >Profit</th>\r\n      <th class=\"right\" >ROI</th>\r\n      <th class=\"right\" >Trading History</th>\r\n    </tr>\r\n  </thead>\r\n  <tbody class = \"table-borderless\">\r\n    <tr\r\n      *ngFor=\"let trader of traders; let i = index\"\r\n      [ngClass]=\"{\r\n          'oddWinner': trader.profitOrLoss > 0 && i % 2 == 1,\r\n          'evenWinner': trader.profitOrLoss > 0 && i % 2 == 0,\r\n          'oddLoser': trader.profitOrLoss < 0 && i % 2 == 1,\r\n          'evenLoser': trader.profitOrLoss < 0 && i % 2 == 0\r\n        }\"\r\n    >\r\n      <!-- <td>{{trader['@type']}}</td> -->\r\n      <td>{{trader.stock}}</td>\r\n      <td class=\"right\" >{{trader.size}}</td>\r\n      <td *ngIf = \"trader['@type'] === '2MA'\" class=\"right\">2MA</td>\r\n      <td *ngIf = \"trader['@type'] === 'BB'\" class=\"right\">BB</td>\r\n      <td *ngIf = \"trader['@type'] === 'PB'\" class=\"right\">PB</td>\r\n      <td *ngIf = \"trader['@type'] === '2MA'\" class=\"right\" ><span> </span>Short: {{trader.lengthShort / 1000}} / Long: {{trader.lengthLong / 1000}}</td>\r\n      <td *ngIf = \"trader['@type'] === 'BB'\" class=\"right\" >Window: {{trader.windowsize}} / Multiplier: {{trader.multiplier}}</td>\r\n      <td *ngIf = \"trader['@type'] === 'PB'\" class=\"right\" >Window: {{trader.windowsize}}</td>\r\n      <td class=\"right\" >{{trader.exitThreshold | percent:'1.2-2'}}</td>\r\n      <td>\r\n        <input\r\n          class=\"btn btn-outline-primary\"\r\n          id=\"trader{{i}}\"\r\n          type=\"button\"\r\n          [value]=\"getState(trader)\"\r\n          [disabled]=\"getState(trader) == 'Stopping' || getState(trader) == 'Starting'\"\r\n          (click)=\"startOrStop($event, false)\" />\r\n      </td>\r\n      <td class=\"right indent\" >{{trader.trades}}</td>\r\n      <td *ngIf=\"trader.trades > 1; else profitNa\" class=\"right\" >{{trader.profitOrLoss | currency:'USD':'symbol':'1.1-1'}}</td>\r\n      <ng-template #profitNa><td class = \"right\">N/A</td></ng-template>\r\n      <td *ngIf=\"trader.trades > 1; else ROINa\" class=\"right\" >{{trader.ROI | percent:'1.2-2'}}</td>\r\n      <ng-template #ROINa><td class = \"right\">N/A</td></ng-template>\r\n      <td>\r\n        <span *ngIf = 'trader.trades > 1; else notrade'>\r\n          <button [disabled] = 'trader.trades <= 1' type=\"button\" class=\"btn btn-outline-primary\" data-toggle=\"modal\" data-target=\".bd-example-modal-lg\" (click)=\"moreInfo($event)\" [value]='trader.ID'>More Info</button>\r\n        </span>\r\n        <ng-template #notrade>\r\n            <button disabled type=\"button\" class=\"btn btn-outline-primary\" data-toggle=\"modal\" data-target=\".bd-example-modal-lg\" (click)=\"moreInfo($event)\" [value]='trader.ID'>No Histories</button>\r\n        </ng-template>\r\n      </td>\r\n    </tr>\r\n   </tbody>\r\n   <tfoot>\r\n      <tr class=\"table-info\">\r\n          <td colspan=\"6\" ></td>\r\n          <td *ngIf = \"getTotalTrades() != null\" class=\"right table-bordered\" >Total Trades</td>\r\n          <td *ngIf = \"getTotalTrades() != null\" class=\"right table-bordered\" >Total Profits</td>\r\n          <td colspan=\"2\" ></td>\r\n      </tr>\r\n     <tr class=\"table-info\">\r\n       <td colspan=\"6\" ></td>\r\n       <td *ngIf = \"getTotalTrades() != null\" class=\"right table-bordered\" >{{getTotalTrades()}}</td>\r\n       <td class=\"right table-bordered\" >{{getTotalProfit() | currency:'USD':'symbol':'1.2-2'}}</td>\r\n       <td colspan=\"2\" ></td>\r\n     </tr>\r\n   </tfoot>\r\n</table>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/view/trader-toolbar.html":
/*!****************************************************************!*\
  !*** ./node_modules/raw-loader!./src/view/trader-toolbar.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id = \"theToolbar\" class = \"createBox\">\r\n  <div class=\"container\">\r\n    <div>\r\n      <h1 style=\"color: #007BFF\">Create a trade</h1>\r\n    </div>\r\n    <div class=\"row\">\r\n        <div class=\"col-3\">\r\n          <span>Investment Type</span><br>\r\n          <div style=\"width: 100%;\" class=\"btn-group\">\r\n              <select [(ngModel)] = \"type\" value=\"{{type}}\" class=\"btn btn-primary dropdown-toggle\" >\r\n                  <option value=\"2MA\" >Two moving averages</option>\r\n                  <option value=\"BB\" >Bollinger bands</option>\r\n                  <option value=\"PB\" >Price Breakout </option>\r\n              </select>\r\n          </div>\r\n\r\n        </div>\r\n        <div class=\"col-2\">\r\n            <span>Stock</span><br>\r\n            <span> </span><input class = \"border border-primary rounded height35\" style=\"width: 100%;\" type=\"text\" [(ngModel)]=\"stock\" />\r\n        </div>\r\n        <div class=\"col-1\">\r\n            <span>Size</span><br>\r\n            <span> </span><input class = \"border border-primary rounded height35\" type=\"number\" step=\"100\" style=\"width: 100%;\" [(ngModel)]=\"size\" />\r\n        </div>\r\n          <div *ngIf = \"type === '2MA'\" class=\"col-1\">\r\n            <span>Short</span><br>\r\n            <input class = \"border border-primary rounded height35\" style=\"width: 100%;\" type=\"number\" min=\"15\" step=\"15\" [(ngModel)]=\"lengthShort\" />\r\n          </div>\r\n          <div *ngIf = \"type === '2MA'\" class=\"col-1\">\r\n            <span>Long</span><br>\r\n            <input class = \"border border-primary rounded height35\" style=\"width: 100%;\" type=\"number\" min=\"15\" step=\"15\" [(ngModel)]=\"lengthLong\" />\r\n          </div>\r\n          <div *ngIf = \"type === 'BB' || type === 'PB'\" class=\"col-2\">\r\n            <span>Window Size</span><br>\r\n            <input class = \"border border-primary rounded height35\" style=\"width: 100%;\" type=\"number\" min=\"15\" step=\"15\" [(ngModel)]=\"windowsize\" />\r\n          </div>\r\n          <div *ngIf = \"type === 'BB'\" class=\"col-1\">\r\n              <span>Multiplier</span><br>\r\n              <input class = \"border border-primary rounded height35\" style=\"width: 100%;\" type=\"number\" min=\"0.1\" max = \"0.5\" step=\"0.05\" [(ngModel)]=\"multiplier\" />\r\n          </div>\r\n\r\n        <div class=\"col-1\">\r\n          <span>Exit</span><br>\r\n          <input class = \"border border-primary rounded height35\" style=\"width: 70%;\" type=\"number\" min=\"0.25\" step=\"0.25\" [(ngModel)]=\"exitThreshold\" /><span>%</span>\r\n        </div>\r\n        <div id = \"createDiv\" class=\"col-2\">\r\n            <input class=\"btn btn-primary createButton\" style=\"width: 100%;\"  type=\"button\" value=\"Create\" (click)=\"create()\" />\r\n        </div>\r\n    </div>\r\n  </div>\r\n\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var routes = [];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* No styles at this level testing git branching */\r\n#topBackground{\r\n    background-color: white;\r\n    margin-left: 2%;\r\n    margin-right: 10%;\r\n}\r\n#titleName{\r\n    padding-top: 10px;\r\n    /* margin-left: 350px; */\r\n    color: #030065;\r\n}\r\n#topImage{\r\n    margin-right: 200px;\r\n}\r\n.titleText {\r\n    text-align: center;\r\n    font-size: 40px;\r\n}\r\n.banner {\r\n  background-color: #85caff;\r\n}\r\n#top {\r\n    background-image: url('background.jpg');\r\n}\r\n#topImage {\r\n    width: 100%;\r\n    z-index: 0;\r\n}\r\n/* Container needed to position the button. Adjust the width as needed */\r\n.container1 {\r\n    position: relative;\r\n  }\r\n/* Make the image responsive */\r\n.container1 img {\r\n    width: 100%;\r\n    height: auto;\r\n  }\r\n/* Style the button and place it in the middle of the container/image */\r\n.container1 .btn {\r\n    position: absolute;\r\n    top: 70%;\r\n    left: 50%;\r\n    -webkit-transform: translate(-50%, -50%);\r\n            transform: translate(-50%, -50%);\r\n    -ms-transform: translate(-50%, -50%);\r\n    background-color: #007BFF;\r\n    color: white;\r\n    font-size: 16px;\r\n    padding: 12px 24px;\r\n    border: none;\r\n    cursor: pointer;\r\n    border-radius: 5px;\r\n    border-color: white;\r\n  }\r\n.container1 .btn:hover {\r\n    background-color: #5eabff;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsa0RBQWtEO0FBQ2xEO0lBQ0ksdUJBQXVCO0lBQ3ZCLGVBQWU7SUFDZixpQkFBaUI7QUFDckI7QUFFQTtJQUNJLGlCQUFpQjtJQUNqQix3QkFBd0I7SUFDeEIsY0FBYztBQUNsQjtBQUdBO0lBQ0ksbUJBQW1CO0FBQ3ZCO0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsZUFBZTtBQUNuQjtBQUVBO0VBQ0UseUJBQXlCO0FBQzNCO0FBR0E7SUFDSSx1Q0FBK0M7QUFDbkQ7QUFFQTtJQUNJLFdBQVc7SUFDWCxVQUFVO0FBQ2Q7QUFFQSx3RUFBd0U7QUFDeEU7SUFDSSxrQkFBa0I7RUFDcEI7QUFFQSw4QkFBOEI7QUFDOUI7SUFDRSxXQUFXO0lBQ1gsWUFBWTtFQUNkO0FBRUEsdUVBQXVFO0FBQ3ZFO0lBQ0Usa0JBQWtCO0lBQ2xCLFFBQVE7SUFDUixTQUFTO0lBQ1Qsd0NBQWdDO1lBQWhDLGdDQUFnQztJQUNoQyxvQ0FBb0M7SUFDcEMseUJBQXlCO0lBQ3pCLFlBQVk7SUFDWixlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLG1CQUFtQjtFQUNyQjtBQUVBO0lBQ0UseUJBQXlCO0VBQzNCIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBObyBzdHlsZXMgYXQgdGhpcyBsZXZlbCB0ZXN0aW5nIGdpdCBicmFuY2hpbmcgKi9cclxuI3RvcEJhY2tncm91bmR7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIG1hcmdpbi1sZWZ0OiAyJTtcclxuICAgIG1hcmdpbi1yaWdodDogMTAlO1xyXG59XHJcblxyXG4jdGl0bGVOYW1le1xyXG4gICAgcGFkZGluZy10b3A6IDEwcHg7XHJcbiAgICAvKiBtYXJnaW4tbGVmdDogMzUwcHg7ICovXHJcbiAgICBjb2xvcjogIzAzMDA2NTtcclxufVxyXG5cclxuXHJcbiN0b3BJbWFnZXtcclxuICAgIG1hcmdpbi1yaWdodDogMjAwcHg7XHJcbn1cclxuXHJcbi50aXRsZVRleHQge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiA0MHB4O1xyXG59XHJcblxyXG4uYmFubmVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjODVjYWZmO1xyXG59XHJcblxyXG5cclxuI3RvcCB7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vYXNzZXRzL2JhY2tncm91bmQuanBnKTtcclxufVxyXG5cclxuI3RvcEltYWdlIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgei1pbmRleDogMDtcclxufVxyXG5cclxuLyogQ29udGFpbmVyIG5lZWRlZCB0byBwb3NpdGlvbiB0aGUgYnV0dG9uLiBBZGp1c3QgdGhlIHdpZHRoIGFzIG5lZWRlZCAqL1xyXG4uY29udGFpbmVyMSB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgfVxyXG4gIFxyXG4gIC8qIE1ha2UgdGhlIGltYWdlIHJlc3BvbnNpdmUgKi9cclxuICAuY29udGFpbmVyMSBpbWcge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IGF1dG87XHJcbiAgfVxyXG4gIFxyXG4gIC8qIFN0eWxlIHRoZSBidXR0b24gYW5kIHBsYWNlIGl0IGluIHRoZSBtaWRkbGUgb2YgdGhlIGNvbnRhaW5lci9pbWFnZSAqL1xyXG4gIC5jb250YWluZXIxIC5idG4ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiA3MCU7XHJcbiAgICBsZWZ0OiA1MCU7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgIC1tcy10cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDdCRkY7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICBwYWRkaW5nOiAxMnB4IDI0cHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBib3JkZXItY29sb3I6IHdoaXRlO1xyXG4gIH1cclxuICBcclxuICAuY29udGFpbmVyMSAuYnRuOmhvdmVyIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM1ZWFiZmY7XHJcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = "Automated Trading Platform";
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-root",
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _view_trader_table__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../view/trader-table */ "./src/view/trader-table.ts");
/* harmony import */ var _view_trader_toolbar__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../view/trader-toolbar */ "./src/view/trader-toolbar.ts");










var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
                _view_trader_table__WEBPACK_IMPORTED_MODULE_8__["TraderTable"],
                _view_trader_toolbar__WEBPACK_IMPORTED_MODULE_9__["TraderToolbar"],
                _view_trader_table__WEBPACK_IMPORTED_MODULE_8__["NgbdModalContent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_4__["AlertModule"].forRoot(),
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"]
            ],
            providers: [],
            exports: [_view_trader_table__WEBPACK_IMPORTED_MODULE_8__["TraderTable"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]],
            entryComponents: [_view_trader_table__WEBPACK_IMPORTED_MODULE_8__["NgbdModalContent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ "./src/model/bollinger-bands.ts":
/*!**************************************!*\
  !*** ./src/model/bollinger-bands.ts ***!
  \**************************************/
/*! exports provided: BollingerBands */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BollingerBands", function() { return BollingerBands; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _trader__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./trader */ "./src/model/trader.ts");


/**
 * Serializable/deserializable encapsulation of a BB strategy,
 * including its trading history, profitability, and ROI.
 *
 * @author Dan Li
 */
var BollingerBands = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](BollingerBands, _super);
    function BollingerBands(ID, stock, size, active, stopping, positions, profitOrLoss, ROI, windowsize, multiplier, exitThreshold) {
        var _this = _super.call(this, "BB", ID, stock, size, active, stopping, positions, profitOrLoss, ROI) || this;
        _this.windowsize = windowsize;
        _this.multiplier = multiplier;
        _this.exitThreshold = exitThreshold;
        return _this;
    }
    return BollingerBands;
}(_trader__WEBPACK_IMPORTED_MODULE_1__["Trader"]));



/***/ }),

/***/ "./src/model/price-breakout.ts":
/*!*************************************!*\
  !*** ./src/model/price-breakout.ts ***!
  \*************************************/
/*! exports provided: PriceBreakers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PriceBreakers", function() { return PriceBreakers; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _trader__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./trader */ "./src/model/trader.ts");


/**
 * Serializable/deserializable encapsulation of a BB strategy,
 * including its trading history, profitability, and ROI.
 *
 * @author Chunwei Cai
 */
var PriceBreakers = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](PriceBreakers, _super);
    function PriceBreakers(ID, stock, size, active, stopping, positions, profitOrLoss, ROI, windowsize, exitThreshold) {
        var _this = _super.call(this, "PB", ID, stock, size, active, stopping, positions, profitOrLoss, ROI) || this;
        _this.windowsize = windowsize;
        _this.exitThreshold = exitThreshold;
        return _this;
    }
    return PriceBreakers;
}(_trader__WEBPACK_IMPORTED_MODULE_1__["Trader"]));



/***/ }),

/***/ "./src/model/trader-service.ts":
/*!*************************************!*\
  !*** ./src/model/trader-service.ts ***!
  \*************************************/
/*! exports provided: TraderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TraderService", function() { return TraderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _two_moving_averages__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./two-moving-averages */ "./src/model/two-moving-averages.ts");
/* harmony import */ var _bollinger_bands__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./bollinger-bands */ "./src/model/bollinger-bands.ts");
/* harmony import */ var _price_breakout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./price-breakout */ "./src/model/price-breakout.ts");





/**
 * A client component for the HFTrader application's TraderService.
 * Also has the responsibility to set a timer and to poll the service
 * for updates on traders and their status and trading history.
 * Subscribe with an implementation of the above TraderUpdate interface,
 * and this component will push updates as it fetches them.
 *
 * @author Will Provost
 */
var TraderService = /** @class */ (function () {
    /**
     * Set up service URL. Bind all methods so our 'this' references
     * make sense. Initialize subscribers array. Start the polling timer.
     */
    function TraderService() {
        this.URL = "/traders";
        this.handler = this.handler.bind(this);
        this.checkResponseCode = this.checkResponseCode.bind(this);
        this.checkOK = this.checkOK.bind(this);
        this.checkCreated = this.checkCreated.bind(this);
        this.getTraders = this.getTraders.bind(this);
        this.getTrader = this.getTrader.bind(this);
        this.setActive = this.setActive.bind(this);
        this.createTrader = this.createTrader.bind(this);
        this.subscribers = [];
        this.start();
    }
    TraderService_1 = TraderService;
    /**
     * Add the subscriber.
     */
    TraderService.prototype.subscribe = function (subscriber) {
        this.subscribers.push(subscriber);
    };
    /**
     * Remove the subscriber.
     */
    TraderService.prototype.unsubscribe = function (subscriber) {
        for (var i = 0; i < this.subscribers.length; ++i) {
            if (this.subscribers[i] === subscriber) {
                this.subscribers.splice(i, 1); //remove element at index i
                break;
            }
        }
    };
    /**
     * Call each subscriber, passing the current traders array.
     */
    TraderService.prototype.notify = function () {
        var _this = this;
        this.getTraders().then(function (traders) {
            for (var _i = 0, _a = _this.subscribers; _i < _a.length; _i++) {
                var subscriber = _a[_i];
                subscriber.latestTraders(traders);
            }
        });
    };
    /**
     * Set a timer to go off in one second and then every 15 seconds therafter,
     * calling our notify() method each time.
     */
    TraderService.prototype.start = function () {
        var _this = this;
        var oneSecond = setInterval(function () {
            _this.notify();
            _this.timer = setInterval(_this.notify.bind(_this), 15000);
            clearInterval(oneSecond);
        }, 1000);
    };
    /**
     * Shut down the polling timer.
     */
    TraderService.prototype.stop = function () {
        clearInterval(this.timer);
    };
    /**
     * Error handler currently just logs to the console.
     * A popup message box or some other UI should come into play at some point.
     */
    TraderService.prototype.handler = function (err) {
        console.log("From trade-service:  " + err);
        return null;
    };
    /**
     * Helper to check that the HTTP response code was the expected alue.
     * Either throws an error or returns the response, making the function
     * suitable for use in a fetch/then chain.
     */
    TraderService.prototype.checkResponseCode = function (response, expected) {
        if (response.status !== expected) {
            throw Error("Unexpected response code: " + response.status);
        }
        return response;
    };
    /**
     * Specialization of checkResponseCode() that expects HTTP 200 OK.
     */
    TraderService.prototype.checkOK = function (response) {
        return this.checkResponseCode(response, 200);
    };
    /**
     * Specialization of checkResponseCode() that expects HTTP 201 Created.
     */
    TraderService.prototype.checkCreated = function (response) {
        console.log("In checkCreated");
        return this.checkResponseCode(response, 201);
    };
    /**
     * Parses the given (weakly typed) object and creates the appropriate
     * type of Trader, holding the appropriate values.
     * Currently, only 2MA traders are supported.
     */
    TraderService.makeTrader = function (source) {
        if (source["@type"] === "2MA") {
            return new _two_moving_averages__WEBPACK_IMPORTED_MODULE_2__["TwoMovingAverages"](source.id, source.stock, source.size, source.active, !source.active && !source.stoppedTrading, source.positions, source.profitOrLoss, source.roi, source.lengthShort, source.lengthLong, source.exitThreshold);
        }
        else if (source["@type"] === "BB") {
            return new _bollinger_bands__WEBPACK_IMPORTED_MODULE_3__["BollingerBands"](source.id, source.stock, source.size, source.active, !source.active && !source.stoppedTrading, source.positions, source.profitOrLoss, source.roi, source.windowsize, source.multiplier, source.exitThreshold);
        }
        else if (source["@type"] === "PB") {
            return new _price_breakout__WEBPACK_IMPORTED_MODULE_4__["PriceBreakers"](source.id, source.stock, source.size, source.active, !source.active && !source.stoppedTrading, source.positions, source.profitOrLoss, source.roi, source.windowsize, source.exitThreshold);
        }
        else {
            throw Error("Unknown trader type: " + source["@type"]);
        }
    };
    /**
     * Calls the HTTP operation and returns a Promise bearing an array of
     * Trader objects.
     */
    TraderService.prototype.getTraders = function () {
        return fetch(this.URL)
            .then(this.checkOK)
            .then(function (response) { return response.json(); })
            .then(function (traders) { return traders.map(TraderService_1.makeTrader); })
            .catch(this.handler);
    };
    /**
     * Calls the HTTP operation and returns a Promise bearing the
     * requested trader object.
     */
    TraderService.prototype.getTrader = function (ID) {
        return fetch(this.URL + "/" + ID)
            .then(this.checkOK)
            .then(function (response) { return response.json(); })
            .then(TraderService_1.makeTrader)
            .catch(this.handler);
    };
    /**
     * Calls the HTTP operation, and on successful response triggers
     * a notify() which in turn will fetch an updated list of traders.
     */
    TraderService.prototype.setActive = function (ID, start) {
        var _this = this;
        return fetch(this.URL + "/" + ID + "/active", {
            method: "PUT",
            body: start ? "true" : "false"
        })
            .then(this.checkOK)
            .then(function (response) { _this.notify(); return response; })
            .catch(this.handler);
    };
    /**
     * Calls the HTTP operation and returns a Promise bearing
     * the newly created trader, which will carry the server-generated ID.
     */
    TraderService.prototype.createTrader = function (trader) {
        var _this = this;
        //connect to java server
        console.log(trader);
        return fetch(this.URL, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
            },
            body: JSON.stringify(trader)
        })
            .then(this.checkCreated)
            .then(function (response) { return response.json(); })
            .then(TraderService_1.makeTrader)
            .then(function (created) { _this.notify(); return created; })
            .catch(this.handler);
    };
    var TraderService_1;
    TraderService = TraderService_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root',
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TraderService);
    return TraderService;
}());



/***/ }),

/***/ "./src/model/trader.ts":
/*!*****************************!*\
  !*** ./src/model/trader.ts ***!
  \*****************************/
/*! exports provided: Trader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Trader", function() { return Trader; });
/**
 * Serializable/deserializable encapsulation of a strategy,
 * including its trading history, profitability, and ROI.
 *
 * @author Will Provost
 */
var Trader = /** @class */ (function () {
    /**
     * Store all of the given information as properties,
     * and pre-compute the total number of trades for use by the HTML template.
     */
    function Trader(typeName, ID, stock, size, active, stopping, positions, profitOrLoss, ROI) {
        this["@type"] = typeName;
        this.ID = ID;
        this.stock = stock;
        this.size = size;
        this.active = active;
        this.stopping = stopping;
        this.positions = positions;
        this.profitOrLoss = profitOrLoss;
        this.ROI = ROI;
        this.trades = positions.length * 2;
        if (positions.length !== 0 && !positions[positions.length - 1].closingTrade) {
            this.trades -= 1;
        }
    }
    return Trader;
}());



/***/ }),

/***/ "./src/model/two-moving-averages.ts":
/*!******************************************!*\
  !*** ./src/model/two-moving-averages.ts ***!
  \******************************************/
/*! exports provided: TwoMovingAverages */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TwoMovingAverages", function() { return TwoMovingAverages; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _trader__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./trader */ "./src/model/trader.ts");


/**
 * Serializable/deserializable encapsulation of a 2MA strategy,
 * including its trading history, profitability, and ROI.
 *
 * @author Will Provost
 */
var TwoMovingAverages = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TwoMovingAverages, _super);
    function TwoMovingAverages(ID, stock, size, active, stopping, positions, profitOrLoss, ROI, lengthShort, lengthLong, exitThreshold) {
        var _this = _super.call(this, "2MA", ID, stock, size, active, stopping, positions, profitOrLoss, ROI) || this;
        _this.lengthShort = lengthShort;
        _this.lengthLong = lengthLong;
        _this.exitThreshold = exitThreshold;
        return _this;
    }
    return TwoMovingAverages;
}(_trader__WEBPACK_IMPORTED_MODULE_1__["Trader"]));



/***/ }),

/***/ "./src/view/popup.css":
/*!****************************!*\
  !*** ./src/view/popup.css ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".wideItem {\r\n    width: 90%;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy92aWV3L3BvcHVwLmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFVBQVU7QUFDZCIsImZpbGUiOiJzcmMvdmlldy9wb3B1cC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIud2lkZUl0ZW0ge1xyXG4gICAgd2lkdGg6IDkwJTtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/view/trader-table.css":
/*!***********************************!*\
  !*** ./src/view/trader-table.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".right {\r\n  text-align: middle;\r\n}\r\n\r\n.oddWinner {\r\n  background-color: #efe;\r\n}\r\n\r\n.evenWinner {\r\n  background-color: #dfd;\r\n}\r\n\r\n.oddLoser {\r\n  background-color: #fee;\r\n}\r\n\r\n.evenLoser {\r\n  background-color: #fdd;\r\n}\r\n\r\ntable {\r\n  border-collapse: collapse;\r\n}\r\n\r\nthead th {\r\n  margin-bottom: 6px;\r\n  padding: 8px 6px;\r\n}\r\n\r\ntbody tr {\r\n  border: 1px solid grey;\r\n}\r\n\r\ntbody td {\r\n  padding: 8px 6px;\r\n}\r\n\r\ntbody td.narrow {\r\n  padding-left: 0;\r\n}\r\n\r\ntbody td.wide {\r\n  padding-left: 12px;\r\n}\r\n\r\ntbody td.indent {\r\n  padding-right: 12px;\r\n}\r\n\r\ntfoot td {\r\n  font-weight: bold;\r\n  margin-top: 8px;\r\n}\r\n\r\ntfoot td.indent {\r\n  padding-right: 12px;\r\n}\r\n\r\ninput[type=\"button\"] {\r\n  width: 72px;\r\n  /* background-color: white; */\r\n  padding: 2px;\r\n}\r\n\r\ninput[type=\"image\"] {\r\n  height: 24px;\r\n  margin-top: 2px;\r\n  border: 2px outset #ccc;\r\n}\r\n\r\n#tableBottom{\r\n  /* margin-left: 5%;\r\n  margin-right: 5%; */\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy92aWV3L3RyYWRlci10YWJsZS5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxzQkFBc0I7QUFDeEI7O0FBRUE7RUFDRSxzQkFBc0I7QUFDeEI7O0FBRUE7RUFDRSxzQkFBc0I7QUFDeEI7O0FBRUE7RUFDRSxzQkFBc0I7QUFDeEI7O0FBRUE7RUFDRSx5QkFBeUI7QUFDM0I7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsZ0JBQWdCO0FBQ2xCOztBQUVBO0VBQ0Usc0JBQXNCO0FBQ3hCOztBQUVBO0VBQ0UsZ0JBQWdCO0FBQ2xCOztBQUVBO0VBQ0UsZUFBZTtBQUNqQjs7QUFFQTtFQUNFLGtCQUFrQjtBQUNwQjs7QUFFQTtFQUNFLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLGlCQUFpQjtFQUNqQixlQUFlO0FBQ2pCOztBQUVBO0VBQ0UsbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0UsV0FBVztFQUNYLDZCQUE2QjtFQUM3QixZQUFZO0FBQ2Q7O0FBRUE7RUFDRSxZQUFZO0VBQ1osZUFBZTtFQUNmLHVCQUF1QjtBQUN6Qjs7QUFFQTtFQUNFO3FCQUNtQjtBQUNyQiIsImZpbGUiOiJzcmMvdmlldy90cmFkZXItdGFibGUuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnJpZ2h0IHtcclxuICB0ZXh0LWFsaWduOiBtaWRkbGU7XHJcbn1cclxuXHJcbi5vZGRXaW5uZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNlZmU7XHJcbn1cclxuXHJcbi5ldmVuV2lubmVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGZkO1xyXG59XHJcblxyXG4ub2RkTG9zZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZWU7XHJcbn1cclxuXHJcbi5ldmVuTG9zZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZGQ7XHJcbn1cclxuXHJcbnRhYmxlIHtcclxuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG59XHJcblxyXG50aGVhZCB0aCB7XHJcbiAgbWFyZ2luLWJvdHRvbTogNnB4O1xyXG4gIHBhZGRpbmc6IDhweCA2cHg7XHJcbn1cclxuXHJcbnRib2R5IHRyIHtcclxuICBib3JkZXI6IDFweCBzb2xpZCBncmV5O1xyXG59XHJcblxyXG50Ym9keSB0ZCB7XHJcbiAgcGFkZGluZzogOHB4IDZweDtcclxufVxyXG5cclxudGJvZHkgdGQubmFycm93IHtcclxuICBwYWRkaW5nLWxlZnQ6IDA7XHJcbn1cclxuXHJcbnRib2R5IHRkLndpZGUge1xyXG4gIHBhZGRpbmctbGVmdDogMTJweDtcclxufVxyXG5cclxudGJvZHkgdGQuaW5kZW50IHtcclxuICBwYWRkaW5nLXJpZ2h0OiAxMnB4O1xyXG59XHJcblxyXG50Zm9vdCB0ZCB7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgbWFyZ2luLXRvcDogOHB4O1xyXG59XHJcblxyXG50Zm9vdCB0ZC5pbmRlbnQge1xyXG4gIHBhZGRpbmctcmlnaHQ6IDEycHg7XHJcbn1cclxuXHJcbmlucHV0W3R5cGU9XCJidXR0b25cIl0ge1xyXG4gIHdpZHRoOiA3MnB4O1xyXG4gIC8qIGJhY2tncm91bmQtY29sb3I6IHdoaXRlOyAqL1xyXG4gIHBhZGRpbmc6IDJweDtcclxufVxyXG5cclxuaW5wdXRbdHlwZT1cImltYWdlXCJdIHtcclxuICBoZWlnaHQ6IDI0cHg7XHJcbiAgbWFyZ2luLXRvcDogMnB4O1xyXG4gIGJvcmRlcjogMnB4IG91dHNldCAjY2NjO1xyXG59XHJcblxyXG4jdGFibGVCb3R0b217XHJcbiAgLyogbWFyZ2luLWxlZnQ6IDUlO1xyXG4gIG1hcmdpbi1yaWdodDogNSU7ICovXHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/view/trader-table.ts":
/*!**********************************!*\
  !*** ./src/view/trader-table.ts ***!
  \**********************************/
/*! exports provided: TraderTable, NgbdModalContent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TraderTable", function() { return TraderTable; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgbdModalContent", function() { return NgbdModalContent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _model_trader_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/trader-service */ "./src/model/trader-service.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");




/**
 * Angular component showing all current traders, including their
 * type, parameters, state, and profitability.
 *
 * @author Will Provost
 */
var TraderTable = /** @class */ (function () {
    /**
     * Store the injected service references.
     */
    function TraderTable(service, modalService) {
        this.modalService = modalService;
        this.traders = [];
        this.traderState = new Map();
        this.service = service;
        service.subscribe(this);
        service.notify();
    }
    /**
     * Helper to format times in mm:ss format.
     */
    TraderTable.prototype.minSec = function (millis) {
        var seconds = millis / 1000;
        var minutes = Math.floor(seconds / 60);
        seconds = seconds % 60;
        var pad = seconds < 10 ? "0" : "";
        return "" + minutes + ":" + pad + seconds;
    };
    /**
     * Replace our array of traders with the latest,
     * determine the state of each trader out of 4 different states and triger a UI update
     */
    TraderTable.prototype.latestTraders = function (traders) {
        var _this = this;
        this.traders = traders;
        this.traders.forEach(function (trader) {
            switch (_this.traderState.get(trader.ID)) {
                case State.STARTING: {
                    _this.traderState.set(trader.ID, trader.active ? State.STARTED : State.STARTING);
                    break;
                }
                case State.STOPPING: {
                    _this.traderState.set(trader.ID, trader.active ? State.STOPPING : State.STOPPED);
                    console.log(trader.positions[0]);
                    break;
                }
                default: {
                    _this.traderState.set(trader.ID, trader.active ? State.STARTED : State.STOPPED);
                    break;
                }
            }
        });
    };
    /**
     * Helper to derive a label for the trader's state: "Started",
     * "Stopped", or, if deactivated but still closing out a position,
     * "Stopping".
     */
    TraderTable.prototype.getState = function (trader) {
        //console.log(trader.ID, this.traderState.get(trader.ID), trader.active)
        return this.traderState.get(trader.ID);
    };
    /**
     * Helper to derive the total number of trades made by this trader.
     */
    TraderTable.prototype.getTotalTrades = function () {
        return this.traders.map(function (t) { return t.trades; }).reduce(function (x, y) { return x + y; }, 0);
    };
    /**
     * Helper to derive the trader's total profit.
     */
    TraderTable.prototype.getTotalProfit = function () {
        return this.traders.map(function (t) { return t.profitOrLoss; }).reduce(function (x, y) { return x + y; }, 0);
    };
    TraderTable.prototype.getAvgROI = function () {
        var totalROI = 0;
        var count = 0;
        // return totalROI / this.traders.length;
        for (var i = 0; i < this.traders.length; i++) {
            //console.log(this.traders[i].ROI);
            if (this.traders[i].trades > 1) {
                totalROI = totalROI + this.traders[i].ROI * 100.0;
                count = count + 1;
                console.log(totalROI);
            }
        }
        if (count == 0)
            return count;
        return totalROI / count;
    };
    /**
     * Finds the trader at the given table index and uses the
     * TraderService component to send an HTTP request to toggle the
     * trader's state.
     */
    TraderTable.prototype.startOrStop = function (ev, hardStop) {
        var index = ev.target.id.replace("trader", "");
        var trader = this.traders[index];
        this.traderState.set(trader.ID, trader.active ? State.STOPPING : State.STARTING);
        this.service.setActive(trader.ID, !trader.active);
    };
    TraderTable.prototype.moreInfo = function (ev) {
        var _this = this;
        var id = Number(ev.target.value);
        console.log(id);
        this.service.getTrader(id).then(function (result) {
            // console.log('test',result.positions[0]);
            _this.allPositions = result.positions;
            console.log(_this.allPositions[0]);
            // console.log("From result: " + this.allPositions[1].toString());
            var modalRef = _this.modalService.open(NgbdModalContent, { size: 'xl' });
            modalRef.componentInstance.name = 'World';
            var posArr = [];
            for (var _i = 0, _a = Object.entries(_this.allPositions); _i < _a.length; _i++) {
                var _b = _a[_i], key = _b[0], value = _b[1];
                modalRef.componentInstance.allPositions.push(value);
            }
            // console.log(posArr);
            // modalRef.componentInstance.allPositions = posArr;
        });
    };
    TraderTable = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "trader-table",
            template: __webpack_require__(/*! raw-loader!./trader-table.html */ "./node_modules/raw-loader/index.js!./src/view/trader-table.html"),
            styles: [__webpack_require__(/*! ./trader-table.css */ "./src/view/trader-table.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_model_trader_service__WEBPACK_IMPORTED_MODULE_2__["TraderService"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"]])
    ], TraderTable);
    return TraderTable;
}());

var State;
(function (State) {
    State["STARTED"] = "Started";
    State["STOPPED"] = "Stopped";
    State["STOPPING"] = "Stopping";
    State["STARTING"] = "Starting";
})(State || (State = {}));
var NgbdModalContent = /** @class */ (function () {
    function NgbdModalContent(activeModal) {
        this.activeModal = activeModal;
        this.allPositions = [];
    }
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NgbdModalContent.prototype, "name", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NgbdModalContent.prototype, "allPositions", void 0);
    NgbdModalContent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ngbd-modal-content',
            template: __webpack_require__(/*! raw-loader!./popup.html */ "./node_modules/raw-loader/index.js!./src/view/popup.html"),
            styles: [__webpack_require__(/*! ./popup.css */ "./src/view/popup.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbActiveModal"]])
    ], NgbdModalContent);
    return NgbdModalContent;
}());



/***/ }),

/***/ "./src/view/trader-toolbar.css":
/*!*************************************!*\
  !*** ./src/view/trader-toolbar.css ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "div {\r\n  /* margin-bottom: 4px;\r\n  margin-top: 4px; */\r\n}\r\n\r\nspan {\r\n  /* margin-left: 6px; */\r\n}\r\n\r\ninput {\r\n  width: 40px;\r\n  margin-left: 2px;\r\n  margin-bottom: 4px;\r\n}\r\n\r\ninput[type=\"number\"] {\r\n  width: 50px;\r\n  text-align: left;\r\n}\r\n\r\ninput[type=\"button\"] {\r\n  width: auto;\r\n  margin-top: 10px;\r\n  padding: 4px;\r\n  background-color: #0069D9;\r\n  border: 2px outset #88f;\r\n}\r\n\r\n.center{\r\n  text-align: center;\r\n}\r\n\r\n#typeId{\r\n  margin-bottom: 0px;\r\n}\r\n\r\n.height35 {\r\n  height: 35px;\r\n}\r\n\r\n.createButton{\r\n  margin-top: 20px;\r\n}\r\n\r\n#createDiv{\r\n  padding-bottom: 0px;\r\n}\r\n\r\n.createBox {\r\n  background-color: white;\r\n  padding-top: 10px;\r\n  padding-bottom: 10px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy92aWV3L3RyYWRlci10b29sYmFyLmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFO29CQUNrQjtBQUNwQjs7QUFFQTtFQUNFLHNCQUFzQjtBQUN4Qjs7QUFFQTtFQUNFLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsa0JBQWtCO0FBQ3BCOztBQUVBO0VBQ0UsV0FBVztFQUNYLGdCQUFnQjtBQUNsQjs7QUFFQTtFQUNFLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLHlCQUF5QjtFQUN6Qix1QkFBdUI7QUFDekI7O0FBRUE7RUFDRSxrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxZQUFZO0FBQ2Q7O0FBRUE7RUFDRSxnQkFBZ0I7QUFDbEI7O0FBRUE7RUFDRSxtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSx1QkFBdUI7RUFDdkIsaUJBQWlCO0VBQ2pCLG9CQUFvQjtBQUN0QiIsImZpbGUiOiJzcmMvdmlldy90cmFkZXItdG9vbGJhci5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJkaXYge1xyXG4gIC8qIG1hcmdpbi1ib3R0b206IDRweDtcclxuICBtYXJnaW4tdG9wOiA0cHg7ICovXHJcbn1cclxuXHJcbnNwYW4ge1xyXG4gIC8qIG1hcmdpbi1sZWZ0OiA2cHg7ICovXHJcbn1cclxuXHJcbmlucHV0IHtcclxuICB3aWR0aDogNDBweDtcclxuICBtYXJnaW4tbGVmdDogMnB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDRweDtcclxufVxyXG5cclxuaW5wdXRbdHlwZT1cIm51bWJlclwiXSB7XHJcbiAgd2lkdGg6IDUwcHg7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxufVxyXG5cclxuaW5wdXRbdHlwZT1cImJ1dHRvblwiXSB7XHJcbiAgd2lkdGg6IGF1dG87XHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxuICBwYWRkaW5nOiA0cHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwNjlEOTtcclxuICBib3JkZXI6IDJweCBvdXRzZXQgIzg4ZjtcclxufVxyXG5cclxuLmNlbnRlcntcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbiN0eXBlSWR7XHJcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG59XHJcblxyXG4uaGVpZ2h0MzUge1xyXG4gIGhlaWdodDogMzVweDtcclxufVxyXG5cclxuLmNyZWF0ZUJ1dHRvbntcclxuICBtYXJnaW4tdG9wOiAyMHB4O1xyXG59XHJcblxyXG4jY3JlYXRlRGl2e1xyXG4gIHBhZGRpbmctYm90dG9tOiAwcHg7XHJcbn1cclxuXHJcbi5jcmVhdGVCb3gge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gIHBhZGRpbmctdG9wOiAxMHB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG59Il19 */"

/***/ }),

/***/ "./src/view/trader-toolbar.ts":
/*!************************************!*\
  !*** ./src/view/trader-toolbar.ts ***!
  \************************************/
/*! exports provided: TraderToolbar */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TraderToolbar", function() { return TraderToolbar; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _model_two_moving_averages__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/two-moving-averages */ "./src/model/two-moving-averages.ts");
/* harmony import */ var _model_trader_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../model/trader-service */ "./src/model/trader-service.ts");
/* harmony import */ var _model_bollinger_bands__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../model/bollinger-bands */ "./src/model/bollinger-bands.ts");
/* harmony import */ var src_model_price_breakout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/model/price-breakout */ "./src/model/price-breakout.ts");






/**
 * Angular component for a toolbar that allows the user to configure
 * and to create new traders. The component logic and the HTML template
 * currently support only the 2MA trader type.
 *
 * @author Will Provost
 */
var TraderToolbar = /** @class */ (function () {
    /**
     * Set default values for all properties, which will flow out to the
     * initial UI via two-way binding.
     */
    function TraderToolbar(service) {
        this.service = service;
        this.type = "2MA";
        this.stock = "MRK";
        this.size = 1000;
        this.lengthShort = 30;
        this.lengthLong = 60;
        this.exitThreshold = 3;
        this.windowsize = 30;
        this.multiplier = 0.15;
    }
    /**
     * Reads the values of form controls via two-way binding.
     * Creates an instance of the trader (only 2MA traders currently supported)
     * and sends it to the server to be created and activated.
     */
    TraderToolbar.prototype.create = function () {
        console.log(this.type);
        if (this.type === "2MA") {
            this.service.createTrader(new _model_two_moving_averages__WEBPACK_IMPORTED_MODULE_2__["TwoMovingAverages"](0, this.stock, this.size, true, false, [], 0, NaN, this.lengthShort * 1000, this.lengthLong * 1000, this.exitThreshold / 100));
        }
        if (this.type === "BB") {
            var bbTrader = new _model_bollinger_bands__WEBPACK_IMPORTED_MODULE_4__["BollingerBands"](1, this.stock, this.size, true, false, [], 0, NaN, this.windowsize * 1000, this.multiplier, this.exitThreshold / 100);
            console.log(bbTrader);
            this.service.createTrader(bbTrader);
        }
        if (this.type === "PB") {
            var pbTrader = new src_model_price_breakout__WEBPACK_IMPORTED_MODULE_5__["PriceBreakers"](1, this.stock, this.size, true, false, [], 0, NaN, this.windowsize * 1000, this.exitThreshold / 100);
            console.log(pbTrader);
            this.service.createTrader(pbTrader);
        }
    };
    TraderToolbar = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "trader-toolbar",
            template: __webpack_require__(/*! raw-loader!./trader-toolbar.html */ "./node_modules/raw-loader/index.js!./src/view/trader-toolbar.html"),
            styles: [__webpack_require__(/*! ./trader-toolbar.css */ "./src/view/trader-toolbar.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_model_trader_service__WEBPACK_IMPORTED_MODULE_3__["TraderService"]])
    ], TraderToolbar);
    return TraderToolbar;
}());



/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Dan\master\teamyellow\HFTraderWeb\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es5.js.map